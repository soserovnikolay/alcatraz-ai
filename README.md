# alcatraz-ai

## Requirements
CMake VERSION 2.8.12 or above <br />
Internet connection when building to download gtest

## Usage

### Build
```
./build
```
### Test
```
cd build/
make test; ./test
```
