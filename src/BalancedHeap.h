/*
 * A simple class that stores arithetic type data and calculates median of that data
 *
 * The class has two main methods insertNum() and getMedian()
 *
 * insertNum() inserts number in one of the two heaps in O(logn)
 * getMedian() gets the median of the numbers stored in the data structure in O(1)
 *
 */


#include <queue>
#include <type_traits>
#include <stdexcept>

using namespace std;

template<class T>
class BalancedHeap {

private:
    priority_queue<T> maxHeap;
    priority_queue<T, vector<T>, greater<T>> minHeap;

public:
    BalancedHeap() {
        if (!is_arithmetic<T>::value) {
            throw invalid_argument("Value for BalancedHeap should be an number value");
        }
    }

    BalancedHeap(const initializer_list<T> listOfNums) {
        if (!is_arithmetic<T>::value) {
            throw invalid_argument("Value for BalancedHeap should be an number value");
        }
        for (const auto& num : listOfNums) {
            this->insertNum(num);
        }
    }

    BalancedHeap(const T num, const size_t size) {
        if (!is_arithmetic<T>::value) {
            throw invalid_argument("Value for BalancedHeap should be an number value");
        }
        for (size_t i = 0; i <= size; i++) {
            this->insertNum(num);
        }
    }

    void insertNum(const T num) {
        if (maxHeap.size() + minHeap.size() < 2) {
            insertFirstOrSecondNum(num);
            return; // Heaps are balanced after pushing 1st or 2nd num
                    // No need to call balance()
        }

        if (num < maxHeap.top()) {
            maxHeap.push(num);
        } else {
            minHeap.push(num);
        }

        balance();
    }

    T getMedian() const {
        if (maxHeap.empty() && minHeap.empty()) {
            throw range_error("Can't call getMedian() on empty BalancedHeap!");
        }
        if (maxHeap.size() == minHeap.size()) {
            return (maxHeap.top() + minHeap.top()) / 2;
        } else {
            return maxHeap.size() > minHeap.size() ? maxHeap.top() : minHeap.top();
        }
    }

private:
    void insertFirstOrSecondNum (const T num) {
        // Special logic for adding the first two values that makes sure
        // bigger num is in minHeap and smaller num is in maxHeap.
        if (maxHeap.empty()) {
            maxHeap.push(num);
            return;
        }

        if (minHeap.empty()) {
            if (num > maxHeap.top()) {
                minHeap.push(num);
            } else {
                minHeap.push(maxHeap.top());
                maxHeap.pop();
                maxHeap.push(num);
            }
            return;
        }

    }
    void balance() {
        if (static_cast<int>(maxHeap.size() - minHeap.size()) > 1) {
            minHeap.push(maxHeap.top());
            maxHeap.pop();
        }

        if (static_cast<int>(minHeap.size() - maxHeap.size()) > 1) {
            maxHeap.push(minHeap.top());
            minHeap.pop();
        }
    }

};
