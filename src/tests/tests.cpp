#include "../BalancedHeap.h"
#include <gtest/gtest.h>

TEST(BalancedHeapTest, findMedianOneNum) {
    BalancedHeap<int> heap {2};
    ASSERT_EQ(heap.getMedian(), 2);
}

TEST(BalancedHeapTest, findMedianTwoNums) {
    BalancedHeap<int> heap {2, 4};;
    ASSERT_EQ(heap.getMedian(), 3);
}

TEST(BalancedHeapTest, findMedian) {
    BalancedHeap<int> heap {12, 3, 5, 7, 4, 19, 26};
    ASSERT_EQ(heap.getMedian(), 7);
}

TEST(BalancedHeapTest, findMedian2) {
    BalancedHeap<int> heap {12, 3, 5, 7, 4, 19, 26, 35, 41, 5 , 7, 15};
    ASSERT_EQ(heap.getMedian(), 9);
}

TEST(BalancedHeapTest, findMedianSameNums) {
    BalancedHeap<int> heap (20, 1000000);
    ASSERT_EQ(heap.getMedian(), 20);
}

TEST(BalancedHeapTest, getMedianEmptyHeap) {
    BalancedHeap<int> heap;
    ASSERT_THROW(heap.getMedian(), range_error);
}

TEST(BalancedHeapTest, getMedianEmptyInitilizerListHeap) {
    BalancedHeap<int> heap {};
    ASSERT_THROW(heap.getMedian(), range_error);
}

TEST(BalancedHeapTest, invalidTypeCharStar) {
    ASSERT_THROW(BalancedHeap<char *> heap, invalid_argument);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
